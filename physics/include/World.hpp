#pragma once

#include <vector>

#include "Object.hpp"

namespace physics
{
    
class World
{
public:
    World();
    World(std::vector<Object> objects);
    World(const World &copy);
    World(World &&move);
    
    void addObject(const Object &object);
    void update(double dt);
private:
    void handleCollisions();
    
    std::vector<Object> objects;
};
    
}
