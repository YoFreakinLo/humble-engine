#pragma once

#include <string>

namespace physics
{
/**
 * @brief The Vector2D class represents a two dimensional vector in the coordinate system
 */
class Vector2D
{
public:
    /**
     * @brief Vector2D - default constructor (sets the coordinates to 0)
     */
    Vector2D();
    /**
     * @brief Vector2D - constructor (sets the coordinates to x and y)
     * @param x - the x coordinate of the vector
     * @param y - the y coordinate of the vector
     */
    Vector2D(double x, double y);
    /**
     * @brief Vector2D - copy constructor (makes a deep copy of the vector argument)
     * @param copy - the vector to be copied
     */
    Vector2D(const Vector2D &copy);
    /**
     * @brief Vector2D - move contructor (moves the argument vector into this and renders it to an "initial state")
     * @param move - the vector to be moved
     */
    Vector2D(Vector2D &&move);
    const Vector2D & operator=(const Vector2D &copy);
    const Vector2D & operator=(Vector2D &&move);
    bool operator==(const Vector2D &to) const;
    bool operator!=(const Vector2D &to) const;
    Vector2D operator+(const Vector2D &with) const;
    const Vector2D & operator+=(const Vector2D &with);
    Vector2D operator-(const Vector2D &subtractor) const;
    const Vector2D & operator-=(const Vector2D &subtractor);
    Vector2D operator-() const;
    double operator*(const Vector2D &with) const; // Dot product
    Vector2D operator*(double scalar) const;
    Vector2D operator*(int scalar) const;
    Vector2D operator*(float scalar) const;
    const Vector2D & operator*=(double scalar);
    const Vector2D & operator*=(int scalar);
    const Vector2D & operator*=(float scalar);
    template<typename T> Vector2D operator/(T scalar) const
    {
        Vector2D vector(x, y);
        if(typeid(T) == typeid(int) || \
        typeid(T) == typeid(double) || \
        typeid(T) == typeid(float))
        {
            Vector2D vector(x, y);
            vector.x /= scalar;
            vector.y /= scalar;
        }
        
        return vector;
    }
    
    template<typename T> const Vector2D & operator/=(T scalar)
    {
        if(typeid(T) == typeid(double) || \
        typeid(T) == typeid(int) || \
        typeid(T) == typeid(float))
        {
            x /= scalar;
            y /= scalar;
        }
        
        return *this;
    }
    virtual ~Vector2D();
    
    double size() const;
    std::string toString() const;

    void invert();
    const Vector2D unit() const;
    void makeUnit();

public:
    double x, y;
};
    
}
