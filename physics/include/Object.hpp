#pragma once

#include "Vector2D.hpp"

namespace physics
{

/**
 * @brief The Object class represents a physical object in space
 */
class Object
{
public:
    Object(bool movable, double mass);
    Object(const Object &copy);
    Object(Object &&move);
    const Object & operator=(const Object &copy);
    const Object & operator=(Object &&move);
    bool operator==(const Object &to) const;
    bool operator!=(const Object &to) const;
    virtual ~Object();
    
    Vector2D getForce() const;
    const Vector2D & getAcceleration() const;
    double getMass() const;
    const Vector2D & getSpeed() const;
    const Vector2D & getPosition() const;
    bool isMovable() const;
    void reactToForce(const Vector2D &force);
    
    void setAcceleration(const Vector2D &acceleration);
    void setSpeed(const Vector2D &speed);
    void setPosition(const Vector2D &position);
    void setMovable(bool movable);
    
    void update(double dt);
    
    std::string toString() const;
private:
    Vector2D acceleration;
    Vector2D speed;
    Vector2D position;
    double mass;
    bool movable;
};

}

