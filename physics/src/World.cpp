#include "World.hpp"

namespace physics
{

World::World()
{
    
}

World::World(const World &copy)
{
    objects = copy.objects;
}

World::World(World &&move)
{
    objects = move.objects;
    move.objects.clear();
}

void World::addObject(const Object &object)
{
    objects.push_back(object);
}

void World::update(double dt)
{
    for(auto &object : objects)
    {
        object.update(dt);
    }
}

void World::handleCollisions()
{
    
}

}
