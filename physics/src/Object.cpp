#include <iostream>

#include "Object.hpp"

namespace physics
{

Object::Object(bool movable, double mass)
{
    this->mass = mass;
    this->movable = movable;
}

Object::Object(const Object &copy)
{
    *this = copy;
}

Object::Object(Object &&move)
{
    *this = std::move(move);
}

Object::~Object()
{
}

const Object & Object::operator=(const Object &copy)
{
    mass = copy.mass;
    acceleration = copy.acceleration;
    speed = copy.speed;
    position = copy.position;
    movable = copy.movable;
    
    return *this;
}

const Object & Object::operator=(Object &&move)
{
    mass = std::move(move.mass);
    acceleration = std::move(move.acceleration);
    speed = std::move(move.speed);
    position = std::move(move.position);
    movable = std::move(move.movable);
    
    return *this;
}

bool Object::operator==(const Object &to) const
{
    return movable == to.movable && mass == to.mass && acceleration == to.acceleration && speed == to.speed && position == to.position;
}

bool Object::operator!=(const Object &to) const
{
    return movable != to.movable || mass != to.mass || acceleration != to.acceleration || speed != to.speed || position != to.position;
}

Vector2D Object::getForce() const
{
    return acceleration * mass;
}

const Vector2D & Object::getAcceleration() const
{
    return acceleration;
}

double Object::getMass() const
{
    return mass;
}

const Vector2D & Object::getSpeed() const
{
    return speed;
}

const Vector2D & Object::getPosition() const
{
    return position;
}

bool Object::isMovable() const
{
    return movable;
}

void Object::reactToForce(const Vector2D &force)
{
    std::cout << (force / mass).toString() << std::endl;
    acceleration += force / mass;
}

void Object::setAcceleration(const Vector2D &acceleration)
{
    this->acceleration = acceleration;
}

void Object::setSpeed(const Vector2D &speed)
{
    this->speed = speed;
}

void Object::setPosition(const Vector2D &position)
{
    this->position = position;
}

void Object::setMovable(bool movable)
{
    this->movable = movable;
}

void Object::update(double dt)
{
    if(movable)
    {
        // std::cout << "speed before: " << speed.toString() << " position before: " << position.toString() << std::endl;
        speed += acceleration * dt;
        position += speed * dt + acceleration * 1/2 * dt * dt;
        // std::cout << "speed after: " << speed.toString() << " position after: " << position.toString() << std::endl;
    }
    acceleration = Vector2D();
}

std::string Object::toString() const
{
    return std::string(std::string("mass: ") + std::to_string(mass) + std::string(" current acceleration: ") + acceleration.toString());
}

}
