#include "../include/Vector2D.hpp"
#include <math.h>

namespace physics
{

Vector2D::Vector2D()
{
    this->x = this->y = 0;
}

Vector2D::Vector2D(double x, double y)
{
    this->x = x;
    this->y = y;
}

Vector2D::Vector2D(const Vector2D &copy)
{
    *this = copy;
}


Vector2D::Vector2D(Vector2D &&move)
{
    *this = std::move(move);
}

const Vector2D & Vector2D::operator=(const Vector2D &copy)
{
    this->x = copy.x;
    this->y = copy.y;
    return *this;
}

const Vector2D & Vector2D::operator=(Vector2D &&move)
{
    this->x = move.x;
    move.x = 0;
    this->y = move.y;
    move.y = 0;
    return *this;
}

bool Vector2D::operator==(const Vector2D &to) const
{
    return this->x == to.x && this->y == to.y;
}

bool Vector2D::operator!=(const Vector2D &to) const
{
    return this->x != to.x || this->y != to.y;
}

Vector2D Vector2D::operator+(const Vector2D &with) const
{
    Vector2D retVal(this->x, this->y);
    retVal.x += with.x;
    retVal.y += with.y;
    return retVal;
}

const Vector2D & Vector2D::operator+=(const Vector2D &with)
{
    this->x += with.x;
    this->y += with.y;
    return *this;
}

const Vector2D & Vector2D::operator-=(const Vector2D &subtractor)
{
    this->x -= subtractor.x;
    this->y -= subtractor.y;
    return *this;
}

Vector2D Vector2D::operator-(const Vector2D &subtractor) const
{
    Vector2D retVal(this->x, this->y);
    retVal.x -= subtractor.x;
    retVal.y -= subtractor.y;
    return retVal;
}

Vector2D Vector2D::operator-() const
{
    return Vector2D(-this->x, -this->y);
}

double Vector2D::operator*(const Vector2D &with) const// Dot product
{
    return this->x * with.x + this->y * with.y;
}

Vector2D Vector2D::operator*(double scalar) const
{
    Vector2D vector(*this);
    vector.x *= scalar;
    vector.y *= scalar;
    return vector;
}

Vector2D Vector2D::operator*(int scalar) const
{
    Vector2D vector(*this);
    vector.x *= scalar;
    vector.y *= scalar;
    return vector;
}

Vector2D Vector2D::operator*(float scalar) const
{
    Vector2D vector(*this);
    vector.x *= scalar;
    vector.y *= scalar;
    return vector;
}

const Vector2D & Vector2D::operator*=(double scalar)
{
    x *= scalar;
    y *= scalar;
    
    return *this;
}

const Vector2D & Vector2D::operator*=(int scalar)
{
    x *= scalar;
    y *= scalar;
    
    return *this;
}

const Vector2D & Vector2D::operator*=(float scalar)
{
    x *= scalar;
    y *= scalar;
    
    return *this;
}

Vector2D::~Vector2D()
{
    
}

double Vector2D::size() const
{
    return sqrt(x * x + y * y);
}


std::string Vector2D::toString() const
{
    return std::string(std::string("x: ") + std::to_string(x) + " y: " + std::to_string(y));
}

void Vector2D::invert()
{
    x = -x;
    y = -y;
}

const Vector2D Vector2D::unit() const
{
    return *this / size();
}

void Vector2D::makeUnit()
{
    if(size())
    {
        *this /= size();
    }
}

}
