#include "ComponentLuaBridge.hpp"

std::map<std::string, bool (*)(const Entity *)> ComponentLuaBridge::hasReg = {};
std::map<std::string, Component * (*)(const Entity *)> ComponentLuaBridge::getReg = {};
std::map<std::string, void (*)(Entity *)> ComponentLuaBridge::addReg = {};

void ComponentLuaBridge::addComponent(Entity *entity, const std::string &componentName)
{
    if(ComponentLuaBridge::addReg[componentName])
    {
        ComponentLuaBridge::addReg[componentName](entity);
    }
}

bool ComponentLuaBridge::hasComponent(Entity *entity, const std::string &componentName)
{
    if(ComponentLuaBridge::hasReg[componentName])
    {
        return ComponentLuaBridge::hasReg[componentName](entity);
    }
    return false;
}

Component * ComponentLuaBridge::getComponent(Entity *entity, const std::string &componentName)
{
    if(ComponentLuaBridge::getReg[componentName])
    {
        return ComponentLuaBridge::getReg[componentName](entity);
    }

    return nullptr;
}
