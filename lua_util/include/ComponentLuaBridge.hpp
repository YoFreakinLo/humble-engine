#pragma once

#include <map>

#include "Entity.hpp"
#include "Component.hpp"

class ComponentLuaBridge
{
private:
    ComponentLuaBridge() = default;
    ~ComponentLuaBridge() = default;

public:
    static void addComponent(Entity *entity, const std::string &componentName);
    static bool hasComponent(Entity *entity, const std::string &componentName);
    static Component * getComponent(Entity *entity, const std::string &componentName);

public:
    static std::map<std::string, bool (*)(const Entity *)> hasReg;
    static std::map<std::string, Component * (*)(const Entity *)> getReg;
    static std::map<std::string, void (*)(Entity *)> addReg;
};
