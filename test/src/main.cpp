#include <iostream>

#include "Game.hpp"

#include "TextureComponent.hpp"
#include "PositionComponent.hpp"
#include "AnimationComponent.hpp"
#include "RenderComponent.hpp"
#include "Entity.hpp"
#include "MapEntity.hpp"
#include "MapPositionComponent.hpp"
#include "PlayerEntity.hpp"
#include "EntityPositionCorrectionComponent.hpp"
#include "PlayerPositionComponent.hpp"
#include "MapPositionCorrectionComponent.hpp"

int main(int argc, char *args[])
{
    std::cout << "Test 1: GameCreation and start" << std::endl;
    try
    {
        Game game = Game("Humble", 1200, 800, false);
        World world;
        Entity entity(&world);
        TextureComponent::Manager::addComponentToEntity(&entity);
        TextureComponent::Manager::getComponentOfEntity(&entity)->setImage("../assets/images/spritesheet.png");
        PositionComponent::Manager::addComponentToEntity(&entity);
        PositionComponent *pc = PositionComponent::Manager::getComponentOfEntity(&entity);
        pc->setPosition({100, 100});
        pc->setSpeed(5);
        pc->enableUpdate(false);
        AnimationComponent::Manager::addComponentToEntity(&entity);
        AnimationFrame frame = AnimationFrame({0, 0, 50, 50}, 60);
        AnimationComponent *ac = AnimationComponent::Manager::getComponentOfEntity(&entity);
        ac->addAnimationFrame(frame);
        frame = AnimationFrame({50, 0, 50, 50}, 60);
        ac->addAnimationFrame(frame);
        RenderComponent::Manager::addComponentToEntity(&entity);

        MapEntity map(&world);
        MapPositionComponent::Manager::addComponentToEntity(&map);
        MapPositionComponent *mpc = MapPositionComponent::Manager::getComponentOfEntity(&map);
        mpc->setPosition(0, 0);
        mpc->setSpeed(5);
        TextureComponent::Manager::addComponentToEntity(&map);
        TextureComponent *tc = TextureComponent::Manager::getComponentOfEntity(&map);
        tc->setImage("../assets/images/map.png");
		EntityPositionCorrectionComponent::Manager::addComponentToEntity(&map);

        RenderComponent::Manager::addComponentToEntity(&map);

        PlayerEntity player(&world);
		PlayerPositionComponent::Manager::addComponentToEntity(&player);
		PlayerPositionComponent *ppc = PlayerPositionComponent::Manager::getComponentOfEntity(&player);
		ppc->setPosition({300, 300});
		ppc->setSpeed(5);
        TextureComponent::Manager::addComponentToEntity(&player);
        tc = TextureComponent::Manager::getComponentOfEntity(&player);
        tc->setImage("../assets/images/dot.png");
		MapPositionCorrectionComponent::Manager::addComponentToEntity(&player);

        RenderComponent::Manager::addComponentToEntity(&player);

        game.setWorld(&world);
        game.start();
        std::cout << "Test passed" << std::endl;

    }
    catch(std::exception &e)
    {
        std::cout << "Test failed" << std::endl;
    }
    return 0;
}
