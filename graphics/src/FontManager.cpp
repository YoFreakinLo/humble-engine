#include "FontManager.hpp"

std::unordered_map<const char *, Font> FontManager::fonts = std::unordered_map<const char *, Font>();


TTF_Font * FontManager::getFont(const char *name, int size)
{
    std::string key = std::string(name) + std::to_string(size);
    try
    {
        return fonts.at(key.c_str()).getFont();
    }
    catch(std::out_of_range &e)
    {
        Font font = Font(name, size);
        fonts.insert(std::pair<const char *, Font>(key.c_str(), std::move(font)));
        return fonts.at(key.c_str()).getFont();
    }
}