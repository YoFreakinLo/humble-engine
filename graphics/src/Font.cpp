#include "Font.hpp"

Font::Font(const char *name, int size) : 
    name(name),
    size(size),
    font(nullptr, TTF_CloseFont)
{
    if(TTF_Font *font = TTF_OpenFont(name, size))
    {
        this->font = std::unique_ptr<TTF_Font, void(*)(TTF_Font *)>(font, TTF_CloseFont);
    }
}

Font::Font(Font &&move) : 
    font(nullptr, TTF_CloseFont)
{
    *this = std::move(move);
}

const Font & Font::operator=(Font &&move)
{
    name = move.name;
    size = move.size;
    font = std::move(move.font);
}

TTF_Font * Font::getFont() const
{
    return font.get();
}

int Font::getSize() const
{
    return size;
}

const char * Font::getName() const
{
    return name;
}