#include "Texture.hpp"

Texture::Texture(const char *name, SDL_Texture *texture) : 
    name(name)
{
    this->texture = std::shared_ptr<SDL_Texture>(texture, SDL_DestroyTexture);
    SDL_QueryTexture(texture, nullptr, nullptr, &width, &height);
}

Texture::Texture(const Texture &copy)
{
    *this = copy;
}

Texture::Texture(Texture &&move)
{
    *this = std::move(move);
}

Texture::~Texture()
{
    name = nullptr;
    width = height = 0;
}

const Texture & Texture::operator=(const Texture &copy)
{
    name = copy.name;
    texture = copy.texture;
    width = copy.width;
    height = copy.height;

    return *this;
}

const Texture & Texture::operator=(Texture &&move)
{
    name = move.name;
    move.name = nullptr;
    texture = std::move(move.texture);
    width = std::move(move.width);
    height = std::move(move.height);

    return *this;
}

const char * Texture::getName() const
{
    return name;
}

SDL_Texture * Texture::getTexture() const
{
    return texture.get();
}

int Texture::getWidth() const
{
    return width;
}

int Texture::getHeight() const
{
    return height;
}
