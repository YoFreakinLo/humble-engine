#include "TextureManager.hpp"

std::unordered_map<const char *, Texture> TextureManager::textures;

const Texture & TextureManager::getTexture(const char *path)
{
    try
    {
        return textures.at(path);;
    }
    catch(std::out_of_range &e)
    {
        if(SDL_Surface *imageSurface = IMG_Load(path))
        {
            if(SDL_Texture *imageTexture = SDL_CreateTextureFromSurface(Globals::renderer, imageSurface))
            {
                Texture texture = Texture(path, imageTexture);
                textures.insert(std::pair<const char *, Texture>(path, texture));
                return textures.at(path);
            }
            else
            {
                std::cerr << "TextureManager::getTexture(const char *path): Couldn't create texture. SDL_Error: " << SDL_GetError() << std::endl;
                throw std::exception();
            }
        }
        else
        {
            std::cerr << "TextureManager::getTexture(const char *path): Couldn't create surface. SDL_Error: " << SDL_GetError() << std::endl;
            throw std::exception();
        }
    }
}

const Texture & TextureManager::getTexture(const char *text, const Font &font, const SDL_Color &color, int wrapWidth)
{
    std::string key = std::string(font.getName()) + std::to_string(font.getSize());
    try
    {
        return textures.at(key.c_str());
    }
    catch(std::out_of_range &e)
    {
        if(SDL_Surface *imageSurface = (wrapWidth) ? TTF_RenderText_Blended_Wrapped(font.getFont(), text, color, wrapWidth) : TTF_RenderText_Solid(font.getFont(), text, color))
        {
            if(SDL_Texture *imageTexture = SDL_CreateTextureFromSurface(Globals::renderer, imageSurface))
            {
                if(textures.emplace(std::piecewise_construct, std::forward_as_tuple(key.c_str()), std::forward_as_tuple(key.c_str(), imageTexture)).second)
                {
                    return textures.at(key.c_str());
                }
            }
            else
            {
                std::cerr << "TextureManager::getTexture(const char *text, const Font &font, const SDL_Color &color, int wrapWidth): Couldn't create texture. SDL_Error: " << SDL_GetError() << std::endl;
                throw std::exception();
            }
        }
        else
        {
            std::cerr << "TextureManager::getTexture(const char *text, const Font &font, const SDL_Color &color, int wrapWidth): Couldn't create surface. SDL_Error: " << SDL_GetError() << std::endl;
            throw std::exception();
        }
    }
}
