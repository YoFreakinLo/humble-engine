#pragma once

#include <memory>

#include <SDL2/SDL_ttf.h>

class Font
{
public:
    Font(const char *name, int size);
    Font(const Font &copy) = delete;
    Font(Font &&move);

public:
    const Font & operator=(const Font &copy) = delete;
    const Font & operator=(Font &&move);

public:
    TTF_Font * getFont() const;
    const char * getName() const;
    int getSize() const;

private:
    std::unique_ptr<TTF_Font, void (*)(TTF_Font *)> font;
    const char *name;
    int size;
};