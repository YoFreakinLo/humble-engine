#pragma once

#include <unordered_map>
#include <iostream>

#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "Texture.hpp"
#include "Font.hpp"
#include "Globals.hpp"

class TextureManager
{
public:
    static const Texture & getTexture(const char *path);
    static const Texture & getTexture(const char *text, const Font &font, const SDL_Color &color, int wrapWidth = 0);

private:
    static std::unordered_map<const char *, Texture> textures;
};