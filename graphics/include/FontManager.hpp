#pragma once

#include <unordered_map>

#include "Font.hpp"

class FontManager
{
public:
    static TTF_Font * getFont(const char *name, int size);

private:
    static std::unordered_map<const char *, Font> fonts;
};