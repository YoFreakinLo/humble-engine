#pragma once

#include <memory>

#include <SDL2/SDL.h>

class Texture
{
public:
    Texture(const char *name, SDL_Texture *texture);
    Texture(const Texture &copy);
    Texture(Texture &&move);
    virtual ~Texture();

public:
    const Texture & operator=(const Texture &copy);
    const Texture & operator=(Texture &&move);

public:
    const char * getName() const;
    SDL_Texture * getTexture() const;
    int getWidth() const;
    int getHeight() const;

private:
    const char *name;
    int width;
    int height;
    std::shared_ptr<SDL_Texture> texture;
};
