#pragma once

#include "Component.hpp"

namespace physics
{
    class Vector2D;
}

class MapPositionCorrectionComponent : public Component
{
	COMPONENT(MapPositionCorrectionComponent);
public:
	MapPositionCorrectionComponent(Entity *owner);
	~MapPositionCorrectionComponent();

public:
	void update() override;
	void draw() const override;

private:
	physics::Vector2D *prevPlayerPosition;
};
