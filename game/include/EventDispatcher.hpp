#pragma once

#include <vector>

#include "EventHandler.hpp"

/**
 * @brief The EventDispatcher class polls the event que of the underlying library when prompted
 * and dispatches every event to the registered handlers. The class needs not to be instantiated
 * because it operates on static level.
 */
class EventDispatcher
{
public:
    /**
     * @brief signUpForEvents - adds an EventHandler object to the signed up "listeners"
     * @param handler - the EventHandler which has to be subscribed for the events
     */
    static void signUpForEvents(EventHandler &handler);
    /**
     * @brief pollEventQue - polls the event que and dispatches all the events to the registered listeners
     */
    static void pollEventQue();

private:
    static std::vector<EventHandler *> handlers;
};
