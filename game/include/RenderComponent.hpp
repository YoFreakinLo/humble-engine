#pragma once

#include "Component.hpp"

class RenderComponent : public Component
{
    COMPONENT(RenderComponent)

public:
    RenderComponent(Entity *owner);
    virtual ~RenderComponent() override;

public:
    void update() override;
    void draw() const override;
};
