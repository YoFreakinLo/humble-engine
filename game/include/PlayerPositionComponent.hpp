#pragma once

#include "PositionComponent.hpp"

#include "Vector2D.hpp"

class PlayerPositionComponent : public PositionComponent
{
	COMPONENT(PlayerPositionComponent);

public:
	PlayerPositionComponent(Entity *owner);
	~PlayerPositionComponent() override;

public:
	void update() override;
	void draw() const override;
};
