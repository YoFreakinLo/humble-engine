#pragma once

#include "EventHandler.hpp"

#include <SDL2/SDL_keycode.h>
#include <map>

/**
 * @brief The KeyMap class is a strange animal. It MUST be instantiated to be able to
 * register it as an EventHandler, but afterwards it MUST be used on static level
 */
class KeyMap : public EventHandler
{
public:
    /**
     * @brief recvEvent - implementing the EventHandler interface; calls the appropriate static functions
     * @param event - the event to handle, got from the EventDispatcher
     */
    void recvEvent(const SDL_Event &event);

public:
    /**
     * @brief isKeyPressed - checks wether a key is pressed at the moment (key down) or not (key up)
     * @param key - the key to be checked (currently the SDLK_* value is used as the key argument)
     * @return bool - true if key is pressed, false if key is released
     */
    static bool isKeyPressed(int key);

private:
    /**
     * @brief registerKey - registers key as pressed in the keys map
     * @param key - the key to be registered as pressed
     */
    static void registerKey(int key);
    /**
     * @brief releaseKey - releases a key (means it's not pressed anymore)
     * @param key - the key to be released
     */
    static void releaseKey(int key);

private:
    static std::map<int, bool> keys;
};
