#pragma once

#include "Component.hpp"
#include "Vector2D.hpp"

/**
 * @brief The PositionComponent class is responsible for handling the movement
 * and the position of the entity. This is NOT an all purpose position component
 * more like a demo level stuff
 *
 * The functions setPosition and setSpeed HAS TO BE CALLED fro this to work.
 */
class PositionComponent : public Component
{
    COMPONENT(PositionComponent)

public:
    PositionComponent(Entity *owner);

public:
    void update() override;
    void draw() const override;

public:
    /**
     * @brief setPosition - sets the position in space with a Vector2D
     * @param position - the position of the entity on screen
     */
    void setPosition(const physics::Vector2D &position);
    /**
     * @brief setPosition - sets the position in space with the x and y coordinates
     * @param x - the x coordinate of the entity on screen
     * @param y - the y coordinate of the entity on screen
     */
    void setPosition(const double &x, const double &y);
	void setVelocity(const physics::Vector2D &velocity);
	void setVelocity(const double &x, const double &y);
    /**
     * @brief setSpeed - set the speed (a number value) of the entity
     * @param speed - the speed (just one number, not velocity) of the entity
     */
    void setSpeed(double speed);

public:
    /**
     * @brief getPosition - returns the current position of the entity in space
     * @return const physics::Vector2D & - the position of the entity on screen
     */
    const physics::Vector2D & getPosition() const;
    const physics::Vector2D & getVelocity() const;
    double getSpeed() const;

public:
    void stop();

protected:
    physics::Vector2D position;
    physics::Vector2D velocity;
    double speed;
};
