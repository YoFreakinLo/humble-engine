#pragma once

#include "PositionComponent.hpp"

#include "Vector2D.hpp"

class MapPositionComponent : public PositionComponent
{
    COMPONENT(MapPositionComponent)

public:
    MapPositionComponent(Entity *owner);
    virtual ~MapPositionComponent() override;

public:
    virtual void update() override;
	virtual void draw() const override;
};
