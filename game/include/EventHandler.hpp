#pragma once

#include <SDL2/SDL.h>

/**
 * @brief The EventHandler class is merely an interface to be used by objects
 * which need to handle events.
 */
class EventHandler
{
public:
    /**
     * @brief recvEvent - is a pure virtual function to be implemented by the
     * classes which inherit from this class.
     * @param event - the event to be handled, got from the EventDispatcher
     */
    virtual void recvEvent(const SDL_Event &event) = 0;
};
