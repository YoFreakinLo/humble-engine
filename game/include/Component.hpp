#pragma once

#include <SDL2/SDL.h>

#include "ComponentManager.hpp"

Uint64 getNewId();

class Entity;

#define COMPONENT(ComponentName) \
public:\
    virtual Uint64 getId() override\
    {\
        static Uint64 id = getNewId();\
        return id;\
    }\
public:\
    class Manager : public ComponentManager<ComponentName>\
    {\
        \
    };\
private:\


template<class T>
Uint64 getComponentId()
{
    return T(nullptr).getId();
}

/**
 * @brief The Component struct is a struct because its just holding
 * DATA to be manipulated || read by a system
 */
class Component
{
public:
    virtual Uint64 getId()
    {
        static Uint64 id = getNewId();
        return id;
    }

public:
    Component(Entity *owner);
    Component(const Component &copy);
    Component(Component &&move);
    virtual ~Component() = default;

public:
    const Component & operator=(const Component &);
    const Component & operator=(Component &&);

public:
    virtual void update() = 0;
    virtual void draw() const = 0;

public:
    const Entity * getOwer() const;
    Entity * getOwner();

public:
    void enableUpdate(bool enable = true);
    void enableDraw(bool enable = true);
    bool isUpdateEnabled() const;
    bool isDrawEnabled() const;

protected:
    Entity *owner;

private:
    bool updateEnabled;
    bool drawEnabled;
};
