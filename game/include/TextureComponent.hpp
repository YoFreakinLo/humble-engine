#pragma once

#include "Component.hpp"
#include "Texture.hpp"

/**
 * @brief The TextureComponent class represents the TEXTURE (image) that needs to be drawn on screen.
 * The TEXTURE MIGHT BE A SPRITESHEET BUT it doesn't HAS to be.
 * The setImage member function HAS TO BE CALLED for it this to work.
 */
class TextureComponent : public Component
{
    COMPONENT(TextureComponent)

public:
    TextureComponent(Entity *owner);

public:
    /**
     * @brief update - does nothing
     */
    void update() override;
    /**
     * @brief draw - gets the animation and the position information and renders the correct texture
     * to the screen
     */
    void draw() const override;

public:
    /**
     * @brief setImage - set the image from which the texture has to be created
     * @param path - path to the image
     */
    void setImage(const char *path);

public:
    /**
     * @brief getTexture - get the SDL_Texture of the image set with setImage
     * @return pointer to an SDL_Texture
     */
    SDL_Texture * getTexture() const;
    /**
     * @brief getWidth - get the width of the image set with setImage
     * @return int - width of texture in pixels
     */
    int getWidth() const;
    /**
     * @brief getHeight - get the height of the image set with setImage
     * @return int - height of the texture in pixels
     */
    int getHeight() const;

private:
    const Texture *texture;
};
