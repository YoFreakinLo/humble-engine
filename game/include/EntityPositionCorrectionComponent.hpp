#pragma once

#include "Component.hpp"

namespace physics
{
	class Vector2D;
}

class EntityPositionCorrectionComponent : public Component
{
	COMPONENT(EntityPositionCorrectionComponent);
public:
	EntityPositionCorrectionComponent(Entity *owner);
	~EntityPositionCorrectionComponent() override;

public:
	void update() override;
	void draw() const override;

private:
	physics::Vector2D *prevPositionOfMap;
};
