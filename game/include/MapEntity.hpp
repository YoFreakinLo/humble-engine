#pragma once
#include "Entity.hpp"

class MapEntity : public Entity
{
public:
    MapEntity(World *world = nullptr);
    ~MapEntity() override;

public:
    void update() override;
    void draw() const override;

};
