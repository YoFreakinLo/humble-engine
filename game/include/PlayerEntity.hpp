#pragma once

#include "Entity.hpp"

class PlayerEntity : public Entity
{
public:
    PlayerEntity(World *world);
    ~PlayerEntity() override;

public:
    void update() override;
    void draw() const override;
};
