#pragma once

#include <memory>
#include <vector>

#include <SDL2/SDL.h>

class World;
class Component;

/**
 * @brief The Entity class is just a colection of components.
 * Really it just holds information about the Components associated with it.
 * There is a restriction on the components: only one of each component type can be added to the entity
 */
class Entity
{
public:
    Entity(World *world = nullptr);
    virtual ~Entity();

public:
    virtual void update();
    virtual void draw() const;

public:
    void addComponent(Uint64 componentID, Component *pointer);
    void removeComponent(Uint64 componentID);
    bool hasComponent(Uint64 componentID) const;

public:
    const World * getWorld() const;
    World * getWorld();
    void changeWorld(World *world);

public:
    const Uint64 id;

private:
    static Uint64 getNewId();

private:
    World *world;
    std::vector<Uint64> components;
    std::vector<const char *> modComponents;
    std::vector<Component *> componentPointers;
};
