#pragma once

#include <SDL2/SDL.h>

/**
 * @brief The Globals class, as the name suggests, is the home of the "global variables".
 * It needs not to be instantiated, BUT the init functions MUST be called before usage
 * AND the destroy function MUST be called before returning form main. Between the
 * two function calls the class variables can be READ. Writing them CHANGES NOTHING OR
 * BREAKS the program (causes bugs, etc.).
 */
class Globals
{
public:
    /**
     * @brief init - initializes the globals
     * @param title - title of the window
     * @param w - width of the window in pixels
     * @param h - height of the window in pixels
     * @param flags - flags to be used while createing the window
     */
    static void init(const char *title, int w, int h, int flags);
    /**
     * @brief destroy - deallocates all allocated globals
     */
    static void destroy();

public:
    static SDL_Window *window;
    static SDL_Renderer *renderer;
    static const char *title;
    static int w;
    static int h;
    static int flags;
};
