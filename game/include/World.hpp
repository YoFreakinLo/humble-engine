#pragma once

#include <map>
#include <SDL2/SDL.h>
#include "ComponentManager.hpp"

class Entity;
class MapEntity;
class PlayerEntity;

using EntityMap = std::map<Uint64, Entity *>;

/**
 * @brief The World class represents the current game world,
 * contains all the actors which are part of the game world
 * and notifies them to update or draw themselves
 */
class World
{
public:
    /**
     * @brief update - updates all the actors
     */
    void update();
    /**
     * @brief draw - draws all the actors
     */
    void draw();
    
public:
    void addEntity(Uint64 entityId, Entity *entity);
    void addEntity(Entity *entity);
    void removeEntity(Uint64 entityId);
    void removeEntity(Entity *entity);

public:
    /**
     * @brief getActor - get preciously added actor based on it's key
     * @param key - the key of the actor to find
     * @return const Actor & - the actor if found, exception thrown otherwise
     */
    const Entity * getEntity(Uint64 id) const;
    /**
     * @brief getActor - get preciously added actor based on it's key
     * @param key - the key of the actor to find
     * @return Actor & - the actor if found, exception thrown otherwise
     */
    Entity * getEntity(Uint64 id);
    /**
     * @brief getActors - get all the actors existing in this world
     * @return const map - the map containing the actors
     */
    const EntityMap & getEntities() const;
    /**
     * @brief getActors - get all the actors existing in this world
     * @return map - the map containing the actors
     */
    EntityMap & getEntities();

    const MapEntity * getMap() const;
    MapEntity * getMap();
    const PlayerEntity * getPlayer() const;
    PlayerEntity * getPlayer();

    void setMap(MapEntity *map);
    void setPlayer(PlayerEntity *player);

private:
    EntityMap entities;
    MapEntity *map;
    PlayerEntity *player;
};
