#pragma once

#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "EventDispatcher.hpp"
#include "EventHandler.hpp"

#include "Globals.hpp"
#include "World.hpp"
#include "KeyMap.hpp"

/**
 * @brief The Game class is representing the game as a whole.
 * BUT! It has to be registered as an eventhandler to the EventDispatcher to be able to handle the quit event.
 * It sets the window and the renderer up, starts the game and controlls the game loop. Further it initializes
 * the KeyMap which exists to be used as the ONLY source for handling keypresses and keyreleases.
 * This class DOESN'T EXIST TO HANDLE GAME LOGIC BUT RATHER TO CREATE AND RUN THE GAME.
 */
class Game : public EventHandler
{
public:
    /**
     * @brief Game - constructs the game
     * @param title - title of the game window
     * @param w - width of the game window in pixels
     * @param h - height of the game window in pixels
     * @param fullscreen - true for fullscreen window, false otherwise
     */
    Game(const char *title, int w, int h, bool fullscreen);
    virtual ~Game();

public:
    /**
     * @brief recvEvent - implementing the EventHandler interfaces' function
     * @param event - the event received from the EventDispatcher
     */
    void recvEvent(const SDL_Event &event) override;

public:
    /**
     * @brief setWorld - sets the current game world
     * @param world - the world to operate on (update, draw)
     */
    void setWorld(World *world);
    
public:
    /**
     * @brief start - starts the game loop.
     * Has to be called after all the initialization took place
     */
    void start();

private:
    /**
     * @brief update - calls update on the current world, to update the game state
     */
    void update();
    /**
     * @brief draw - notifies the current world, that it's time to draw the graphics
     */
    void draw() const;

private:
    KeyMap keyMap;

private:
    World *world;
    bool quit;
};
