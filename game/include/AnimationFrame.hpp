#pragma once

#include <SDL2/SDL.h>

/**
 * @brief The AnimationFrame class represents a frame (a clip, a rectangle, whatever)
 * of a spritehseet AND the duration for which the frame has to be
 * displayed. Remember ONE frame of animation!
 */
class AnimationFrame
{
public:
    /**
     * @brief AnimationFrame - contructor
     * @param frame - the rectangle IN THE SPRITESHEET which is a frame of the animation
     * @param duration - the duration in ticks (update count) for how much the current frame should be displayed
     */
    AnimationFrame(const SDL_Rect &frame, Uint32 duration);
    /**
     * @brief AnimationFrame - copy constructor
     * @param copy - the AnimationFrame to be copied
     */
    AnimationFrame(const AnimationFrame &copy);
    /**
     * @brief AnimationFrame - move contructor
     * @param move - the AnimationFrame to be moved
     */
    AnimationFrame(AnimationFrame &&move);

public:
    const AnimationFrame & operator=(const AnimationFrame &copy);
    const AnimationFrame & operator=(AnimationFrame &&move);

public:
    /**
     * @brief update - on every update it increments the current duration
     * when it's heigher then the duration specified in the constructor, the frame
     * is finished
     */
    void update();
    /**
     * @brief finished - tells wether the frame is finished or not
     * @return bool - true if finished, false otherwise
     */
    bool finished() const;
    /**
     * @brief reset - resets the current duration. This way tha frame will not finished anymore
     * until the updated duration times;
     */
    void reset();

public:
    /**
     * @brief getAnimationFrame - returns the clip of the texture representing the current frame of animation
     * @return cosnt SDL_Rect & - the rectangle that represents one frame of animation
     */
    const SDL_Rect & getAnimationFrame() const;
    /**
     * @brief getDuration - returns the specified duration at construction
     * @return Uint32 - the duration for which the current frame should be displayed
     */
    Uint32 getDuration() const;
    /**
     * @brief getCurrentDuration - returns the current duration for which the frame was displayed
     * @return Uint32 - the current duration of displaying the frame
     */
    Uint32 getCurrentDuration() const;

private:
    SDL_Rect frame;
    Uint32 duration;
    Uint32 currentDuration;
};
