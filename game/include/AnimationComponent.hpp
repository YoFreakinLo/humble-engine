#pragma once

#include "Component.hpp"
#include "ComponentManager.hpp"
#include <vector>
#include "AnimationFrame.hpp"

/**
 * @brief The AnimationComponent class is a reeeeeally simple (demo level)
 * animation ... stuff. It takes 1 or more AnimationFrame objects and makes them tick.
 * If one AnimationFrame was displayed for the specified time, AnimationComponent
 * steps to the next AnimationFrame.
 *
 * The function addAnimationFrame HAS TO BE CALLED 1 or more time for this to work.
 */
class AnimationComponent : public Component
{
    COMPONENT(AnimationComponent)

public:
    AnimationComponent(Entity *owner);

public:
    void update() override;
    void draw() const override;

public:
    /**
     * @brief addAnimationFrame - registers a new AnimationFrame
     * @param frame - the frame to be registered
     */
    void addAnimationFrame(const AnimationFrame &frame);

public:
    /**
     * @brief getAnimationFrame - returns the current AnimationFrame as a const reference
     * @return const AniamationFrame & - the current AnimationFrame
     */
    const AnimationFrame & getAnimationFrame() const;

private:
    std::vector<AnimationFrame> frames;
    int currentIndex;
};
