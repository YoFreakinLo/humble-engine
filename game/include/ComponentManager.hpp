#pragma once

#include <vector>

#include "Entity.hpp"

template<class T>
Uint64 getComponentId();

template<typename T>
class ComponentManager
{
public:
    static std::vector<T> & getComponents()
    {
        return ComponentManager<T>::components;
    }

    static void addComponentToEntity(Entity *entity)
    {
        components.push_back(std::make_unique<T>(entity));
        entity->addComponent(getComponentId<T>(), components.back().get());
    }

    static T * getComponentOfEntity(const Entity *entity)
    {
        for(auto &component : components)
        {
            if(component->getOwner() == entity)
            {
                return component.get();
            }
        }
        return nullptr;
    }

    static void removeComponentOfEntity(const Entity *entity)
    {
        auto it = components.begin();
        for(it; it != components.end(); it++)
        {
            if(it->getOwner() == entity)
            {
                break;
            }
        }
        if(it != components.end())
        {
            components.erase(it);
        }
    }

private:
    ComponentManager();
    ~ComponentManager();

private:
    static std::vector<std::unique_ptr<T>> components;
};

template<typename T>
std::vector<std::unique_ptr<T>> ComponentManager<T>::components = {};
