#include "EventDispatcher.hpp"

std::vector<EventHandler *> EventDispatcher::handlers;

void EventDispatcher::signUpForEvents(EventHandler &handler)
{
    handlers.push_back(&handler);
}

void EventDispatcher::pollEventQue()
{
    SDL_Event event;
    while(SDL_PollEvent(&event))
    {
        for(EventHandler *handler : handlers)
        {
            handler->recvEvent(event);
        }
    }
}