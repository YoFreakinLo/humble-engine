#include "PlayerPositionComponent.hpp"

#include "MapPositionComponent.hpp"
#include "World.hpp"
#include "MapEntity.hpp"
#include "TextureComponent.hpp"
#include "KeyMap.hpp"
#include "Globals.hpp"

PlayerPositionComponent::PlayerPositionComponent(Entity *owner) :
	PositionComponent (owner)
{
	enableDraw(false);
}

PlayerPositionComponent::~PlayerPositionComponent()
{

}

void PlayerPositionComponent::update()
{
	char up = -static_cast<char>(KeyMap::isKeyPressed(SDLK_UP)),
		 down = static_cast<char>(KeyMap::isKeyPressed(SDLK_DOWN)),
		 right = static_cast<char>(KeyMap::isKeyPressed(SDLK_RIGHT)),
		 left = -static_cast<char>(KeyMap::isKeyPressed(SDLK_LEFT));
	velocity.x = left * speed + right * speed;
	velocity.y = up * speed + down * speed;

	TextureComponent *tc = TextureComponent::Manager::getComponentOfEntity(owner->getWorld()->getMap());
	int mapWidth = tc->getWidth(), mapHeight = tc->getHeight();
	tc = TextureComponent::Manager::getComponentOfEntity(owner);
	int playerWidth = tc->getWidth(), playerHeight = tc->getHeight();

	MapPositionComponent *mpc = MapPositionComponent::Manager::getComponentOfEntity(owner->getWorld()->getMap());
	physics::Vector2D mapPosition = mpc->getPosition();

	int xCenter = Globals::w / 2 - playerWidth / 2, yCenter = Globals:: h / 2 - playerHeight / 2;
	bool leftEdge = mapPosition.x == 0,
		 rightEdge = mapPosition.x + mapWidth == Globals::w,
		 topEdge = mapPosition.y == 0,
		 bottomEdge = mapPosition.y + mapHeight == Globals::h;

	bool leftToCenter = position.x < xCenter,
		 rightToCenter = position.x > xCenter,
		 higherToCenter = position.y < yCenter,
		 lowerToCenter = position.y > yCenter;


	// Map is on the edge, player wants to move towards the edge
	if((leftEdge && velocity.x < 0) || (rightEdge && velocity.x > 0))
	{
		position.x += velocity.x;
	}
	if((topEdge && velocity.y < 0) || (bottomEdge && velocity.y > 0))
	{
		position.y += velocity.y;
	}
	// Map is on the edge, player wants to move back to center
	if(leftToCenter && velocity.x > 0)
	{
		position.x = position.x + velocity.x < xCenter ? position.x + velocity.x : xCenter;
	}
	if(rightToCenter && velocity.x < 0)
	{
		position.x = position.x + velocity.x > xCenter ? position.x + velocity.x : xCenter;
	}
	if(higherToCenter && velocity.y > 0)
	{
		position.y = position.y + velocity.y < yCenter ? position.y + velocity.y : yCenter;
	}
	if(lowerToCenter && velocity.y < 0)
	{
		position.y = position.y + velocity.y > yCenter ? position.y + velocity.y : yCenter;
	}

	// Correcting position
	position.x = position.x < 0 ? 0 : position.x;
	position.y = position.y < 0 ? 0 : position.y;
	position.x = position.x + playerWidth > Globals::w ? Globals::w - playerWidth : position.x;
	position.y = position.y + playerHeight > Globals::h ? Globals::h - playerHeight : position.y;
}

void PlayerPositionComponent::draw() const
{

}
