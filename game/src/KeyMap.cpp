#include "KeyMap.hpp"

#include <iostream>

std::map<int, bool> KeyMap::keys;

void KeyMap::recvEvent(const SDL_Event &event)
{
    if(event.type == SDL_KEYDOWN)
    {
        registerKey(event.key.keysym.sym);
    }
    else if(event.type == SDL_KEYUP)
    {
        releaseKey(event.key.keysym.sym);
    }
}

bool KeyMap::isKeyPressed(int key)
{
    try
    {
        return keys.at(key);
    }
    catch(std::out_of_range &e)
    {
        return false;
    }
}

void KeyMap::registerKey(int key)
{
    keys[key] = true;
}

void KeyMap::releaseKey(int key)
{
    keys[key] = false;
}
