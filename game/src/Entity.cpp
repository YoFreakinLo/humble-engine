#include "Entity.hpp"

#include "World.hpp"
#include "Component.hpp"

Entity::Entity(World *world) :
    id(getNewId()),
    world(world)
{
    if(world)
    {
        world->addEntity(this);
    }
}

Entity::~Entity()
{

}

void Entity::update()
{
    for(auto *c : componentPointers)
    {
        if(c->isUpdateEnabled())
        {
            c->update();
        }
    }
}

void Entity::draw() const
{
    for(auto *c : componentPointers)
    {
        if(c->isDrawEnabled())
        {
            c->draw();
        }
    }
}

void Entity::addComponent(Uint64 componentId, Component *pointer)
{
    components.push_back(componentId);
    componentPointers.push_back(pointer);
}

void Entity::removeComponent(Uint64 componentId)
{
    auto it = components.begin();
    for(it; it != components.end(); it++)
    {
        if(*it == componentId)
        {
            break;
        }
    }
    if(it != components.end())
    {
        components.erase(it);
    }

    auto i = componentPointers.begin();
    for(i; i != componentPointers.end(); i++)
    {
        if((*i)->getId() == componentId)
        {
            break;
        }
    }
    if(i != componentPointers.end())
    {
        componentPointers.erase(i);
    }
}

bool Entity::hasComponent(Uint64 componentId) const
{
    for(auto c : components)
    {
        if(c == componentId)
        {
            return true;
        }
    }
    return false;
}

const World *Entity::getWorld() const
{
    return world;
}

World * Entity::getWorld()
{
    return world;
}

void Entity::changeWorld(World *world)
{
    if(this->world)
    {
        this->world->removeEntity(this);
    }
    this->world = world;
    if(world)
    {
        world->addEntity(id, this);
    }
}

Uint64 Entity::getNewId()
{
    static Uint64 id = 0;
    return id++;
}
