#include "MapPositionComponent.hpp"
#include "KeyMap.hpp"
#include "Globals.hpp"
#include "TextureComponent.hpp"
#include "World.hpp"
#include "PlayerEntity.hpp"
#include "PositionComponent.hpp"

#include <iostream>

MapPositionComponent::MapPositionComponent(Entity *owner) :
	PositionComponent (owner)
{
    enableDraw(false);
}


MapPositionComponent::~MapPositionComponent()
{

}

void MapPositionComponent::update()
{
    char up = static_cast<char>(KeyMap::isKeyPressed(SDLK_UP)),
         down = -static_cast<char>(KeyMap::isKeyPressed(SDLK_DOWN)),
         left = static_cast<char>(KeyMap::isKeyPressed(SDLK_LEFT)),
         right = -static_cast<char>(KeyMap::isKeyPressed(SDLK_RIGHT));

    velocity.x = left * speed + right *speed;
    velocity.y = up * speed + down * speed;

    velocity.makeUnit();
	velocity *= speed;
    position += velocity;
    // Safeguarding top and left side of the map preventing it to penetrate the screen more than to its edgees
    position.x = position.x > 0 ? 0 : position.x;
    position.y = position.y > 0 ? 0 : position.y;
    TextureComponent *tc = TextureComponent::Manager::getComponentOfEntity(owner);
    int width = tc->getWidth(), height = tc->getHeight();
    double rightEdge = position.x + width,
           bottomEdge = position.y + height;
    //Safeguarding right and bottom sides of the map preventing it to penetrate the screen more tha to its edges
    position.x += rightEdge < Globals::w ? Globals::w - rightEdge : 0;
    position.y += bottomEdge < Globals::h ? Globals::h - bottomEdge : 0;
}

void MapPositionComponent::draw() const
{

}
