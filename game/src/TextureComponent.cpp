#include "TextureComponent.hpp"

#include "TextureManager.hpp"

TextureComponent::TextureComponent(Entity *owner) :
    Component(owner)
{

}

void TextureComponent::update()
{

}

void TextureComponent::draw() const
{

}

void TextureComponent::setImage(const char *path)
{
    texture = &TextureManager::getTexture(path);
}

SDL_Texture * TextureComponent::getTexture() const
{
    return texture->getTexture();
}

int TextureComponent::getWidth() const
{
    return texture->getWidth();
}

int TextureComponent::getHeight() const
{
    return texture->getHeight();
}

//static bool regged = []
//{
//    ComponentLuaBridge::hasReg["TextureComponent"] = [](const Entity *e) -> bool
//    {
//        return e->hasComponent(getComponentId<TextureComponent>());
//    };

//    ComponentLuaBridge::getReg["TextureComponent"] = [](const Entity *e) -> Component *
//    {
//        return TextureComponentManager::getComponentOfEntity(*e);
//    };

//    ComponentLuaBridge::addReg["TextureComponent"] = [](Entity *e) -> void
//    {
//        TextureComponentManager::addComponentToEntity(*e);
//    };

//    return true;
//}();
