#include "PlayerEntity.hpp"
#include "World.hpp"

PlayerEntity::PlayerEntity(World *world) :
    Entity (world)
{
    getWorld()->removeEntity(this->id);
    world->setPlayer(this);
}

PlayerEntity::~PlayerEntity()
{

}

void PlayerEntity::update()
{
    Entity::update();
}

void PlayerEntity::draw() const
{
    Entity::draw();
}
