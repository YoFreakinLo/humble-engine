#include "RenderComponent.hpp"

#include "TextureComponent.hpp"
#include "PositionComponent.hpp"
#include "MapPositionComponent.hpp"
#include "PlayerPositionComponent.hpp"
#include "AnimationComponent.hpp"
#include "Texture.hpp"
#include "Globals.hpp"

RenderComponent::RenderComponent(Entity *owner) :
    Component(owner)
{

}

RenderComponent::~RenderComponent()
{

}

void RenderComponent::update()
{

}

void RenderComponent::draw() const
{
    if(owner->hasComponent(getComponentId<TextureComponent>()))
    {
        TextureComponent *tc = TextureComponent::Manager::getComponentOfEntity(owner);
        SDL_Texture *texture = tc->getTexture();
        int w = tc->getWidth();
        int h = tc->getHeight();

        SDL_Rect srcRect = {0, 0, w, h};
        SDL_Rect dstRect = srcRect;

		PositionComponent *pc = nullptr;
        if(owner->hasComponent(getComponentId<PositionComponent>()))
        {
			pc = PositionComponent::Manager::getComponentOfEntity(owner);
        }
        else if(owner->hasComponent(getComponentId<MapPositionComponent>()))
        {
			pc = MapPositionComponent::Manager::getComponentOfEntity(owner);
        }
		else if(owner->hasComponent(getComponentId<PlayerPositionComponent>()))
		{
			pc = PlayerPositionComponent::Manager::getComponentOfEntity(owner);
		}
		dstRect.x = pc->getPosition().x;
		dstRect.y = pc->getPosition().y;

        if(owner->hasComponent(getComponentId<AnimationComponent>()))
        {
            AnimationComponent *ac = AnimationComponent::Manager::getComponentOfEntity(owner);
            srcRect = ac->getAnimationFrame().getAnimationFrame();
            dstRect.w = srcRect.w;
            dstRect.h = srcRect.h;
        }

        SDL_RenderCopy(Globals::renderer, texture, &srcRect, &dstRect);
    }
}
