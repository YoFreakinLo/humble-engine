#include "Component.hpp"
#include <utility>

Uint64 getNewId()
{
    static Uint64 ID = 0;
    return ID++;
}

Component::Component(Entity *owner) :
    owner(owner),
    updateEnabled(true),
    drawEnabled(true)
{

}

Component::Component(const Component &copy) :
    owner(copy.owner),
    updateEnabled(copy.updateEnabled),
    drawEnabled(copy.drawEnabled)
{

}

Component::Component(Component &&move) :
    owner(move.owner),
    updateEnabled(move.updateEnabled),
    drawEnabled(move.drawEnabled)
{
    move.owner = nullptr;
    move.updateEnabled = move.drawEnabled = false;
}

const Component &Component::operator=(const Component &copy)
{
    owner = copy.owner;
    updateEnabled = copy.updateEnabled;
    drawEnabled = copy.drawEnabled;
    return *this;
}

const Component &Component::operator=(Component &&move)
{
    owner = move.owner;
    move.owner = nullptr;
    updateEnabled = move.updateEnabled;
    drawEnabled = move.drawEnabled;
    move.updateEnabled = move.drawEnabled = false;
    return *this;
}

const Entity *Component::getOwer() const
{
    return owner;
}

Entity *Component::getOwner()
{
    return owner;
}

void Component::enableUpdate(bool enable)
{
    updateEnabled = enable;
}

void Component::enableDraw(bool enable)
{
    drawEnabled = enable;
}

bool Component::isUpdateEnabled() const
{
    return updateEnabled;
}

bool Component::isDrawEnabled() const
{
    return drawEnabled;
}
