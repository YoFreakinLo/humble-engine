#include "MapEntity.hpp"
#include "World.hpp"

MapEntity::MapEntity(World *world) :
    Entity (world)
{
    getWorld()->removeEntity(this->id);
    getWorld()->setMap(this);
}

MapEntity::~MapEntity()
{

}

void MapEntity::update()
{
    Entity::update();
}

void MapEntity::draw() const
{
    Entity::draw();
}
