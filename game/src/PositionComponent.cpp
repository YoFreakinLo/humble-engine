#include "PositionComponent.hpp"

PositionComponent::PositionComponent(Entity *owner) :
    Component(owner)
{

}

void PositionComponent::update()
{
    velocity.makeUnit();
    velocity *= speed;
    position += velocity;
}

void PositionComponent::draw() const
{

}

void PositionComponent::setPosition(const physics::Vector2D &position)
{
    this->position = position;
}

void PositionComponent::setPosition(const double &x, const double &y)
{
    this->position = {x, y};
}

void PositionComponent::setSpeed(double speed)
{
    this->speed = fabs(speed);
}

const physics::Vector2D & PositionComponent::getPosition() const
{
    return position;
}

const physics::Vector2D &PositionComponent::getVelocity() const
{
    return velocity;
}

double PositionComponent::getSpeed() const
{
    return speed;
}

void PositionComponent::stop()
{
    velocity = {};
}

//static bool regged = []
//{
//    ComponentLuaBridge::hasReg["PositionComponent"] = [](const Entity *e) -> bool
//    {
//        return e->hasComponent(getComponentId<PositionComponent>());
//    };

//    ComponentLuaBridge::getReg["PositionComponent"] = [](const Entity *e) -> Component *
//    {
//        return PositionComponentManager::getComponentOfEntity(*e);
//    };

//    ComponentLuaBridge::addReg["PositionComponent"] = [](Entity *e) -> void
//    {
//        PositionComponentManager::addComponentToEntity(*e);
//    };

//    return true;
//}();
