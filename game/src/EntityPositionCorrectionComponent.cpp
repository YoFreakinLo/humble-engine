#include "EntityPositionCorrectionComponent.hpp"
#include "World.hpp"
#include "PositionComponent.hpp"
#include "MapPositionComponent.hpp"
#include "MapEntity.hpp"

EntityPositionCorrectionComponent::EntityPositionCorrectionComponent(Entity *owner) :
	Component(owner),
	prevPositionOfMap(nullptr)
{
	enableDraw(false);
}


EntityPositionCorrectionComponent::~EntityPositionCorrectionComponent()
{
	if(prevPositionOfMap)
	{
		delete prevPositionOfMap;
	}
}

void EntityPositionCorrectionComponent::update()
{
	MapPositionComponent *mpc = MapPositionComponent::Manager::getComponentOfEntity(owner->getWorld()->getMap());
	if(!prevPositionOfMap)
	{
		prevPositionOfMap = new physics::Vector2D();
		return;
	}
	for(auto &pair : owner->getWorld()->getEntities())
	{
		PositionComponent *pc = PositionComponent::Manager::getComponentOfEntity(pair.second);
		physics::Vector2D positionDifference = *prevPositionOfMap - mpc->getPosition();
		*prevPositionOfMap = mpc->getPosition();
		pc->setPosition(pc->getPosition() - positionDifference);
	}
}

void EntityPositionCorrectionComponent::draw() const
{

}

