#include "MapPositionCorrectionComponent.hpp"

#include "MapPositionComponent.hpp"
#include "PlayerPositionComponent.hpp"
#include "World.hpp"
#include "MapEntity.hpp"

MapPositionCorrectionComponent::MapPositionCorrectionComponent(Entity *owner) :
    Component(owner),
    prevPlayerPosition(nullptr)
{
	enableDraw(false);
}

MapPositionCorrectionComponent::~MapPositionCorrectionComponent()
{

}

void MapPositionCorrectionComponent::update()
{
	PlayerPositionComponent *ppc = PlayerPositionComponent::Manager::getComponentOfEntity(owner);
	if(!prevPlayerPosition)
	{
		prevPlayerPosition = new physics::Vector2D(ppc->getPosition());
		return;
	}
	MapPositionComponent *mpc = MapPositionComponent::Manager::getComponentOfEntity(owner->getWorld()->getMap());
	physics::Vector2D positionDifference = *prevPlayerPosition - ppc->getPosition();
	*prevPlayerPosition = ppc->getPosition();
	mpc->setPosition(mpc->getPosition() - positionDifference);
}

void MapPositionCorrectionComponent::draw() const
{

}
