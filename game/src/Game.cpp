#include "Game.hpp"

#include <iostream>

Game::Game(const char *title, int w, int h, bool fullscreen) : 
    world(nullptr),
    quit(true)
{
    if(SDL_Init(SDL_INIT_EVERYTHING))
    {
        std::cerr << "Game::Game(...): Couldn't initialize SDL2. SDL_Error: " << SDL_GetError() << std::endl;
        throw std::exception();
    }
    if(TTF_Init())
    {
        std::cerr << "Game::Game(...): Couldn't initialize SDL2 ttf. TTF_Error: " << TTF_GetError() << std::endl;
        throw std::exception();
    }
    int imageFlags = IMG_INIT_JPG;
    if(IMG_Init(imageFlags) != imageFlags)
    {
        std::cerr << "Game::Game(...): Couldn't initialize SDL2 img. IMG_Error: " << TTF_GetError() << std::endl;
        throw std::exception();
    }
    Uint32 flags = (fullscreen) ? SDL_WINDOW_FULLSCREEN : 0;
    Globals::init(title, w, h, flags);
    EventDispatcher::signUpForEvents(keyMap);
    EventDispatcher::signUpForEvents(*this);
}

Game::~Game()
{
    Globals::destroy();
    IMG_Quit();
    TTF_Quit();
    SDL_Quit();
}

void Game::recvEvent(const SDL_Event &event)
{
    if(event.type == SDL_QUIT)
    {
        quit = true;
        return;
    }
    if(event.type == SDL_KEYDOWN)
    {
        if(event.key.keysym.sym == SDLK_ESCAPE)
        {
            quit = true;
            return;
        }
    }
}

void Game::setWorld(World *world)
{
    this->world = world;
}

void Game::start()
{
    constexpr double cycleTime = 1000 / 60;
    quit = false;
    while(!quit)
    {
        Uint32 loopStart = SDL_GetTicks();
        EventDispatcher::pollEventQue();
        update();
        draw();
        int elapsedTime = SDL_GetTicks() - loopStart;
        if(elapsedTime < cycleTime)
        {
            SDL_Delay(cycleTime - elapsedTime);
        }
    }
}

void Game::update()
{
    world->update();
}

void Game::draw() const
{
    SDL_RenderClear(Globals::renderer);
    world->draw();
    SDL_RenderPresent(Globals::renderer);
}
