#include "AnimationComponent.hpp"
#include "ComponentLuaBridge.hpp"

AnimationComponent::AnimationComponent(Entity *owner) :
    Component(owner),
    currentIndex(0)
{

}

void AnimationComponent::update()
{
    AnimationFrame &currentFrame = frames[currentIndex];
    if(!currentFrame.finished())
    {
        currentFrame.update();
    }
    else
    {
        currentFrame.reset();
        currentIndex++;
        currentIndex = (currentIndex < frames.size()) ? currentIndex : 0;
    }
}

void AnimationComponent::draw() const
{

}

void AnimationComponent::addAnimationFrame(const AnimationFrame &frame)
{
    frames.push_back(frame);
}

const AnimationFrame & AnimationComponent::getAnimationFrame() const
{
    return frames.at(currentIndex);
}

//static bool regger = []() -> bool
//{
//    ComponentLuaBridge::hasReg["AnimationComponent"] = [](const Entity *e) -> bool
//    {
//        return e->hasComponent(getComponentId<AnimationComponent>());
//    };

//    ComponentLuaBridge::getReg["AnimationComponent"] = [](const Entity *e) -> Component *
//    {
//        return AnimationComponentManager::getComponentOfEntity(*e);
//    };

//    ComponentLuaBridge::addReg["AnimationComponent"] = [](Entity *e) -> void
//    {
//        AnimationComponentManager::addComponentToEntity(*e);
//    };
//    return true;
//}();
