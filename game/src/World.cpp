#include "World.hpp"

#include "Entity.hpp"
#include "MapEntity.hpp"
#include "PlayerEntity.hpp"

void World::update()
{
    map->update();
    for(auto &pair : entities)
    {
        pair.second->update();
    }
    player->update();
}

void World::draw()
{
    map->draw();
    for(auto &pair : entities)
    {
        pair.second->draw();
    }
    player->draw();
}

void World::addEntity(Uint64 id, Entity *entity)
{
    std::pair<Uint64, Entity *> pair;
    pair.first = id;
    pair.second = entity;
    entities.insert(pair);
}

void World::addEntity(Entity *entity)
{
    addEntity(entity->id, entity);
}

void World::removeEntity(Uint64 entityId)
{
    auto it = entities.begin();
    for(it; it != entities.end(); it++)
    {
        if(it->second->id == entityId)
        {
            break;
        }
    }
    if(it != entities.end())
    {
        entities.erase(it);
    }
}

void World::removeEntity(Entity *entity)
{
    removeEntity(entity->id);
}

const Entity * World::getEntity(Uint64 id) const
{
    try
    {
        return const_cast<const Entity *>(entities.at(id));
    }
    catch(std::out_of_range &e)
    {
        throw e;
    }
}

Entity * World::getEntity(Uint64 id)
{
    try
    {
        return entities.at(id);
    }
    catch(std::out_of_range &e)
    {
        throw e;
    }
}

const EntityMap & World::getEntities() const
{
    return entities;
}

EntityMap & World::getEntities()
{
    return entities;
}

const MapEntity * World::getMap() const
{
    return map;
}

MapEntity * World::getMap()
{
    return map;
}

const PlayerEntity * World::getPlayer() const
{
    return player;
}

PlayerEntity * World::getPlayer()
{
    return player;
}

void World::setMap(MapEntity *map)
{
    this->map = map;
}

void World::setPlayer(PlayerEntity *player)
{
    this->player = player;
}
