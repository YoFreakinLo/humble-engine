#include "AnimationFrame.hpp"

#include <utility>

AnimationFrame::AnimationFrame(const SDL_Rect &frame, Uint32 duration) : 
    frame(frame),
    duration(duration),
    currentDuration(0)
{

}

AnimationFrame::AnimationFrame(const AnimationFrame &copy)
{
    *this = copy;
}

AnimationFrame::AnimationFrame(AnimationFrame && move)
{
    *this = std::move(move);
}

const AnimationFrame & AnimationFrame::operator=(const AnimationFrame &copy)
{
    frame = copy.frame;
    duration = copy.duration;
    currentDuration = copy.currentDuration;

    return *this;
}

const AnimationFrame & AnimationFrame::operator=(AnimationFrame &&move)
{
    frame = std::move(move.frame);
    duration = std::move(move.duration);
    currentDuration = std::move(move.currentDuration);

    return *this;
}

void AnimationFrame::update()
{
    currentDuration++;
}

bool AnimationFrame::finished() const
{
    return currentDuration >= duration;
}

void AnimationFrame::reset()
{
    currentDuration = 0;
}

const SDL_Rect & AnimationFrame::getAnimationFrame() const
{
    return frame;
}

Uint32 AnimationFrame::getDuration() const
{
    return duration;
}

Uint32 AnimationFrame::getCurrentDuration() const
{
    return currentDuration;
}
