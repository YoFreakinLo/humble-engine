#include "Globals.hpp"

#include <iostream>

SDL_Window * Globals::window = nullptr;
SDL_Renderer * Globals::renderer = nullptr;
const char * Globals::title = nullptr;
int Globals::w = 0;
int Globals::h = 0;
int Globals::flags = 0;

void Globals::init(const char *title, int w, int h, int flags)
{
    Globals::window = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, w, h, flags);
    if(!Globals::window)
    {
        std::cerr << "Globals::init(...): Couldn't create window. SDL_Error: " << SDL_GetError() << std::endl;
        throw std::exception();
    }
    Globals::renderer = SDL_CreateRenderer(Globals::window, -1, SDL_RENDERER_ACCELERATED);
    if(!Globals::renderer)
    {
        SDL_DestroyWindow(Globals::window);
        window = nullptr;
        std::cerr << "Globals::init(...): Couldn't create renderer. SDL_Error: " << SDL_GetError() << std::endl;
        throw std::exception();
    }

    Globals::title = title;
    Globals::w = w;
    Globals::h = h;
    Globals::flags = flags;
}

void Globals::destroy()
{
    if(renderer)
    {
        SDL_DestroyRenderer(renderer);
        renderer = nullptr;
    }

    if(window)
    {
        SDL_DestroyWindow(window);
        window = nullptr;
    }
    title = nullptr;
    w = 0;
    h = 0;
    flags = 0;
}